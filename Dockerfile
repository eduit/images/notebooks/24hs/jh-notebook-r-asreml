ARG CI_PROJECT_DIR

FROM registry.ethz.ch/eduit/images/notebooks/24hs/jh-notebook-r-asreml:baseimage-4.1.5-01

USER root

# R libs
#
# RcppEigen generates huge amounts of output. Install first and mute this step
RUN Rscript >/dev/null 2>&1 -e "install.packages(pkgs=c( \
    'desplot', \
    'ggplot2', \
    'lme4', \
    'lmerTest', \
    'emmeans', \
    'pbkrtest', \
    'MESS', \
    'multcomp', \
    'vcd', \
    'MASS', \
    'vegan', \
    'nlme', \
    'R.methodsS3', \
    'R.oo', \
    'tidyverse'), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)"
    # \
    # && \
    # Rscript -e "install.packages(pkgs=c('BiocManager'), repos=c('http://cran.r-project.org'))" \
    # && \
    # Rscript -e "BiocManager::install()" \
    # && \
    # Rscript -e "devtools::install_github(repo = 'AECP-ETHZ/ETH.OLP')"

# Hard code path not ideal, but good enough for now
RUN Rscript -e "install.packages('/builds/eduit/images/notebooks/24hs/jh-notebook-r-asreml/asreml_4.2.0.302_linux-intel64_R4.3.0.tar.gz', repos = NULL, type='source')" \
  && \
  Rscript -e "install.packages('/builds/eduit/images/notebooks/24hs/jh-notebook-r-asreml/DiGGer_1.0.5_R_x86_64-redhat-linux-gnu.tar.gz', repos = NULL, type='source')"

# RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install -U --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
#   yolov5 \
#   pillow \
#   rawpy \
#   geojson

USER 1000
